<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function index(Type $var = null)
    {
        return view('sections.body');
    }
}
