<!DOCTYPE html>
<html lang="en">
<!-- ------Head-Tag------ -->
@include('includes.head')

<body >
<!-- ------Section------ -->

@yield('content')

</body>

</html>
