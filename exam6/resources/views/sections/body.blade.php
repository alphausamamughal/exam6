@extends('layouts.index')
@section('content')
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 bg-img">
                <div class="d-flex justify-content-center align-items-center align-items-center h-100">
                  <h1>LOGO HERE</h1>
                </div>
            </div>
            <div class="col-md-5 mt-5 p-5 ">
                <h1 class="text-center">THANK YOU</h1>
                <h2 class="text-center">FOR YOUR SIGNUP REQUEST</h2>
                <p class="text-center text-muted">Please provide us with some additional information <br> <span>in order to complete your profile</span></p>
                <!-- form -->
                <form class="row g-3 sign-up">
                    <div class="col-md-4">
                      <!-- <input type="text" class="form-control" id="inputEmail4" placeholder="Month"> -->
                      <input type="text" class="form-control rounded-0 mb-3 text-uppercase ps-0 rounded-0" id="exampleFormControlInput1" placeholder="Month">
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control rounded-0 mb-3" id="" placeholder="Day">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control rounded-0 mb-3" id=""placeholder="Year">
                      </div>
                    <div class="col-12">
                      <input type="text" class="form-control rounded-0 mb-3" id="" placeholder="ID Type*">
                    </div>
                    <div class="col-12">
                      <input type="number" class="form-control rounded-0 mb-3" id="" placeholder="ID Number">
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control rounded-0 mb-3" id="" placeholder="Country of Residence">
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control rounded-0 mb-3" id="" placeholder="Country of Citizenship">
                      </div>
                      <div class="col-md-6">
                       <a href="" class="text-decoration-none text-muted under-line"><u>Continue Later</u></a>
                      </div>
                    <div class="col-md-6">
                      <button type="button" class="btn btn-primary btn-color px-5">Continue</button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</section>


@endsection
